-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

wireshark-2.6.2.tar.xz: 28392140 bytes
SHA256(wireshark-2.6.2.tar.xz)=49b2895ee3ba17ef9ef0aebfdc4d32a778e0f36ccadde184516557d5f3357094
RIPEMD160(wireshark-2.6.2.tar.xz)=e9b782d49d9a063ba556320e9f2c08dea079967d
SHA1(wireshark-2.6.2.tar.xz)=52517c30926211b0b718815b51a3f06a18d8f5da

Wireshark-win64-2.6.2.exe: 59963968 bytes
SHA256(Wireshark-win64-2.6.2.exe)=88aa2ca018090fc73ffb273aa1ba9f690ec06deb77d1ec7ff9b39fe646ca2877
RIPEMD160(Wireshark-win64-2.6.2.exe)=3b947ada3e64bfb1c1b16a470926d94ed9db391b
SHA1(Wireshark-win64-2.6.2.exe)=90217eb0ed020a53a9ae80682c0881d347d11b4a

Wireshark-win32-2.6.2.exe: 54249888 bytes
SHA256(Wireshark-win32-2.6.2.exe)=3d886e435570b7326f53d00996040ef65b9e2a5bffe48645ce29ea5a23930801
RIPEMD160(Wireshark-win32-2.6.2.exe)=c2c5afa101559976439f36401ea1cc4564fa624e
SHA1(Wireshark-win32-2.6.2.exe)=eb7c50e80d6e7ec834599c1facfd6a3fd66aebf8

Wireshark-win32-2.6.2.msi: 43728896 bytes
SHA256(Wireshark-win32-2.6.2.msi)=99d5d94345a20e177736533840ff59859a76e864247a8146a73fca227f004043
RIPEMD160(Wireshark-win32-2.6.2.msi)=7f21412e4d335f6e797356b968fbef14afb03b8c
SHA1(Wireshark-win32-2.6.2.msi)=05f1f9c4b9bed8c4447e5e31f907c578f52cf067

Wireshark-win64-2.6.2.msi: 49364992 bytes
SHA256(Wireshark-win64-2.6.2.msi)=381076d09c757038072f761f7eee9d5aa45fa8423b771ba34ddbd8b56f2c429c
RIPEMD160(Wireshark-win64-2.6.2.msi)=a080eec0f8bd089f493d0c76837d7fe03c1fa0dd
SHA1(Wireshark-win64-2.6.2.msi)=2c6b5bf555729d1e5ee3a1dda8d2b14d3bb01759

WiresharkPortable_2.6.2.paf.exe: 37482552 bytes
SHA256(WiresharkPortable_2.6.2.paf.exe)=d36727bdb8cc3a72bfb80084d3c634c3bfa4661f4de68d644b43ef5d41c52b69
RIPEMD160(WiresharkPortable_2.6.2.paf.exe)=a98756bf5a67e47e1ca9ecd8836f2e6913a56f27
SHA1(WiresharkPortable_2.6.2.paf.exe)=dd11e62f34212be77abee9d2227a2fd3b613b0a5

Wireshark 2.6.2 Intel 64.dmg: 169012317 bytes
SHA256(Wireshark 2.6.2 Intel 64.dmg)=ef54b04a73df4069e29e77bc1940f3b767ee498c4e28f739eabda78ef71ab4a9
RIPEMD160(Wireshark 2.6.2 Intel 64.dmg)=f93d2cc4057337ca76d1aa435b0039a60927bebb
SHA1(Wireshark 2.6.2 Intel 64.dmg)=3a46de720848b286e7c115c75c7b00bcd08155aa

You can validate these hashes using the following commands (among others):

    Windows: certutil -hashfile Wireshark-win64-x.y.z.exe SHA256
    Linux (GNU Coreutils): sha256sum wireshark-x.y.z.tar.xz
    macOS: shasum -a 256 "Wireshark x.y.z Intel 64.dmg"
    Other: openssl sha256 wireshark-x.y.z.tar.xz
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEWlrbp9vqbD+HIk8ZgiRKeOb+ruoFAltPqKQACgkQgiRKeOb+
rurNbg//dw5903/0W2vw1a6u8F9JVvXfctb9/t1IOD2yT2omPXFTfqEkcwcY5c8W
FoSsflHM6g4rf8jqpqyipSPb6lYRJjm1fZGDzTilVPe+pcAV/HZ2QSdwOgw9FiAs
sV2eZdqPMVqdeLgDGtC4aHHabwsytFNaWtZLVyKr4ojdUfJNIBa40iUrItxXfgxA
GDCnVpdapuygk4rMeDpi3qZtvEKmgZ9Yj5aseX+wBYIT21EShP/gHSKNSA8x3gGz
xnpvOrz2qyJmWB6sBmIQndEXrYdazKr14Fzhmc2ajFMOJLwTGIZg5wl+UDnmPikW
6R1gRzSwkjEtgTKlZ9Gcel8eg6fNjW9HC9d4VjZzG4N693YrYwlpu0FIvaK+QGxE
yEJKPJnlaCi37Q6GBiKIpC5NUkTnt38Gb5DJ4/N3tk4P2LGlSyyMxLc5U096Zd8V
KCE/OVUuZs/4NsgIYaTYWDyTeNjjN2ZXnyx0N3x8yzWHcB6gYVPJc2lKouZe9XqZ
9Gz1Fr0/LEbx+r0iFOEm9pX/W8a5pzZnMn5YYUeTue61ZZp/yBOf7oTqjCVvSPHU
rZhsHMLcZnBNFoYKr03dcvukgSNsndTJPXvAEIX9FVmQUcQAEsdXRFO/csihG7l/
7KWgNjReI7eoWkBUH8sx7J+4wZVy9leWjHTtkZKTeOo6OO1vJx4=
=OiJq
-----END PGP SIGNATURE-----
