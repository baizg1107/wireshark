Name:           wireshark
Version:        2.6.2
Release:        13
Epoch:          1
Summary:        Network traffic analyzer
License:        GPL+
URL:            http://www.wireshark.org/
Source0:        https://wireshark.org/download/src/all-versions/%{name}-%{version}.tar.xz
Source1:        https://www.wireshark.org/download/src/all-versions/SIGNATURES-%{version}.txt

Patch0001:      wireshark-0006-Move-tmp-to-var-tmp.patch
Patch0002:      wireshark-0007-cmakelists.patch

Patch6000:      wireshark-CVE-2018-16057.patch
Patch6001:      wireshark-CVE-2018-16058.patch
Patch6002:      wireshark-CVE-2018-18225.patch
Patch6003:      wireshark-CVE-2018-18226.patch
Patch6004:      wireshark-CVE-2018-18227.patch
Patch6005:      wireshark-CVE-2018-19622.patch
Patch6006:      Replace-lbmpdm_fetch_uintN_encoded-with-tvb_get_guin.patch
Patch6007:      wireshark-CVE-2018-19623.patch
Patch6008:      wireshark-CVE-2018-19624.patch
Patch6009:      wireshark-CVE-2018-19625.patch
Patch6010:      wireshark-CVE-2018-19626.patch
Patch6011:      wireshark-CVE-2018-19627.patch
Patch6012:      wireshark-CVE-2018-19628.patch
Patch6013:      wireshark-CVE-2019-9208.patch
Patch6014:      wireshark-CVE-2019-9209.patch
Patch6015:      wireshark-CVE-2019-5718.patch
Patch6016:      CVE-2019-10894.patch
Patch6017:      CVE-2019-10896.patch
Patch6018:      CVE-2019-10899.patch
Patch6019:      CVE-2019-10901.patch
Patch6020:      CVE-2019-10903.patch
Patch6021:      CVE-2019-10895.patch
Patch6022:      CVE-2019-5716.patch
Patch6023:      CVE-2019-5717.patch
Patch6024:      CVE-2019-5719.patch
Patch6025:      CVE-2020-11647.patch
Patch6026:      fix-hash-table-key-memory-corruption.patch
Patch6027:      CVE-2020-13164.patch
Patch6028:      CVE-2020-15466.patch
Patch6029:      CVE-2018-16056.patch
Patch6030:      CVE-2020-25862.patch
Patch6031:      CVE-2020-25863.patch
Patch6032:      wireshark-initialize-point-in-end_string.patch

Requires(pre):  shadow-utils
Requires(post): systemd-udev
Requires:       %{name}-cli = %{epoch}:%{version}-%{release} xdg-utils hicolor-icon-theme
BuildRequires:  bzip2-devel c-ares-devel elfutils-devel gcc-c++ glib2-devel gnutls-devel gtk3-devel krb5-devel libcap-devel
BuildRequires:  libgcrypt-devel libnl3-devel libpcap-devel >= 0.9 libselinux-devel libsmi-devel openssl-devel desktop-file-utils
BuildRequires:  xdg-utils  bison flex pcre-devel perl(Pod::Html) perl(Pod::Man) libssh-devel qt5-linguist qt5-qtbase-devel
BuildRequires:  qt5-qtmultimedia-devel qt5-qtsvg-devel zlib-devel git cmake
Provides:       %{name}-cli = %{epoch}:%{version}-%{release}
Obsoletes:      %{name}-cli < %{epoch}:%{version}-%{release} wireshark-qt wireshark-gtk

%description
Wireshark is an open source tool for profiling network traffic and analyzing
packets. Such a tool is often referred to as a network analyzer, network
protocol analyzer or sniffer.

Wireshark, formerly known as Ethereal, can be used to examine the details of
traffic at a variety of levels ranging from connection-level information to
the bits that make up a single packet. Packet capture can provide a network
administrator with information about individual packets such as transmit time,
source, destination, protocol type and header data. This information can be
useful for evaluating security events and troubleshooting network security
device issues.

%package        devel
Summary:        Development headers and libraries for wireshark
Requires:       %{name} = %{epoch}:%{version}-%{release} glibc-devel glib2-devel

%description    devel
The wireshark-devel package includes header files and libraries necessary
for the wireshark library.

%package        help
Summary:        This package contains help documents
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description    help
Files for help with wireshark.

%prep
%autosetup -n %{name}-%{version} -S git -p1

%build
%cmake -G "Unix Makefiles"  -DDISABLE_WERROR=ON  -DBUILD_wireshark=ON  -DENABLE_QT5=ON  -DENABLE_LUA=OFF \
-DBUILD_mmdbresolve=OFF  -DBUILD_randpktdump=OFF -DBUILD_androiddump=OFF -DENABLE_SMI=ON -DENABLE_PORTAUDIO=OFF \
-DENABLE_PLUGINS=ON -DENABLE_NETLINK=ON -DBUILD_dcerpcidl2wrs=OFF
%make_build

%install
%make_install
desktop-file-validate %{buildroot}%{_datadir}/applications/wireshark.desktop
install -d -m 0755  %{buildroot}%{_includedir}/wireshark
IDIR="%{buildroot}%{_includedir}/wireshark"
install -d "${IDIR}/epan/crypt" "${IDIR}/epan/ftypes" "${IDIR}/epan/dfilter" "${IDIR}/epan/dissectors"
install -d "${IDIR}/epan/wmem" "${IDIR}/wiretap" "${IDIR}/wsutil"
install -d %{buildroot}%{_udevrulesdir}
install -m 644 config.h epan/register.h cfile.h file.h ws_symbol_export.h ws_diag_control.h "${IDIR}/"
install -m 644 epan/*.h                 "${IDIR}/epan/"
install -m 644 epan/crypt/*.h           "${IDIR}/epan/crypt"
install -m 644 epan/ftypes/*.h          "${IDIR}/epan/ftypes"
install -m 644 epan/dfilter/*.h         "${IDIR}/epan/dfilter"
install -m 644 epan/dissectors/*.h      "${IDIR}/epan/dissectors"
install -m 644 epan/wmem/*.h            "${IDIR}/epan/wmem"
install -m 644 wiretap/*.h              "${IDIR}/wiretap"
install -m 644 wsutil/*.h               "${IDIR}/wsutil"
touch %{buildroot}%{_bindir}/%{name}
%delete_la

%pre
getent group wireshark >/dev/null || groupadd -r wireshark
getent group usbmon >/dev/null || groupadd -r usbmon

%post
/sbin/ldconfig
/usr/bin/udevadm trigger --subsystem-match=usbmon

%postun
/sbin/ldconfig

%files
%{_datadir}/appdata/%{name}.appdata.xml
%{_datadir}/applications/wireshark.desktop
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/icons/hicolor/*/mimetypes/*
%{_datadir}/mime/packages/wireshark.xml
%doc COPYING
%attr(0750, root, wireshark) %caps(cap_net_raw,cap_net_admin=ep) %{_bindir}/dumpcap
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_libdir}/wireshark/extcap/*
%{_libdir}/wireshark/*.cmake
%{_libdir}/wireshark/plugins/2.6/epan/*.so
%{_libdir}/wireshark/plugins/2.6/wiretap/*.so
%{_libdir}/wireshark/plugins/2.6/codecs/*.so
%{_datadir}/wireshark/*

%files devel
%{_includedir}/wireshark
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/%{name}.pc

%files help
%doc AUTHORS INSTALL NEWS README* doc/README.* ChangeLog
%{_mandir}/man?/*

%changelog
* Mon Nov 02 2020 lingsheng <lingsheng@huawei.com> - 2.6.2-13
- Fix use-of-uninitialized-value in end_string

* Wed Oct 21 2020 wangxiao <wangxiao65@huawei.com> - 2.6.2-12
- Type:cves
- ID: CVE-2020-25862 CVE-2020-25863
- SUG:NA
- DESC: fix CVE-2020-25862 CVE-2020-25863

* Wed Sep 15 2020 wangxiao <wangxiao65@huawei.com> - 2.6.2-11
- Type:cves
- ID: CVE-2018-16056
- SUG:restart
- DESC: fix CVE-2018-16056

* Thu Sep 10 2020 baizhonggui <baizhonggui@huawei.com> - 2.6.2-10
- Modify source0

* Wed Aug 05 2020 yaokai <yaokai13@huawei.com> - 2.6.2-9
- Type:cves
- ID: CVE-2020-15466
- SUG:restart
- DESC: fix CVE-2020-15466

* Tue Jul 21 2020 wangyue <wangyue92@huawei.com> - 2.6.2-8
- Type:cves
- ID: CVE-2020-13164
- SUG:restart
- DESC: fix CVE-2020-13164

* Fri May 15 2020 huanghaitao <huanghaitao8@huawei.com> - 2.6.2-7
- Type:cves
- ID: CVE-2020-11647 
- SUG:restart
- DESC: fix CVE-2020-11647

* Sun Feb 2 2020 lingyang <lingyang2@huawei.com> - 2.6.2-6
- Type:cves
- ID: CVE-2019-5719
- SUG:restart
- DESC: fix CVE-2019-5719

* Sun Feb 2 2020 gulining<gulining1@huawei.com> - 2.6.2-5
- Type:cves
- ID: CVE-2019-5716 CVE-2019-5717
- SUG:restart
- DESC: fix CVE-2019-5716 CVE-2019-5717

* Wed Dec 25 2019 gulining <gulining1@huawei.com> - 2.6.2-4
- Type:cves
- ID: CVE-2019-10894 CVE-2019-10896 CVE-2019-10899 CVE-2019-10901 CVE-2019-10903 CVE-2019-10895
- SUG:restart
- DESC: fix CVE-2019-10894 CVE-2019-10896 CVE-2019-10899 CVE-2019-10901 CVE-2019-10903 CVE-2019-10895

* Mon Nov 25 2019 gulining<gulining1@huawei.com> - 2.6.2-3
- revise obsoletes

* Wed Nov 13 2019 gulining<gulining1@huawei.com> - 2.6.2-2
- Pakcage init
